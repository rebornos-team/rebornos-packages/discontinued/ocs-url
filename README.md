# ocs-url

An install helper program for items served via OpenCollaborationServices (ocs://).

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/ocs-url.git
```

